﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.IO;
using System.Collections.Generic;

namespace LocalAds
{
	public class LocalAdsController : MonoBehaviour {
		#region LinkingObject
		[SerializeField]
		private LocalAdsHolder holder;
		#endregion



		private List<Sprite> bannerSprites = new List<Sprite>();
		private LocalAdsModel localAds;
		public bool Load(){
			Manager.GetInstance();
			localAds = getLocalAds();
			bannerSprites.Clear();
			if (localAds == null) {
				return false;
			}

			var _paths = localAds.GetImagesPath();
			for (int i = 0; i < _paths.Count; i++) {
				var _texture = new Texture2D(1, 1);
				_texture.LoadImage(File.ReadAllBytes(Application.persistentDataPath + "/" + _paths[i]));
				bannerSprites.Add(Sprite.Create(_texture, new Rect(0, 0, _texture.width, _texture.height), new Vector2(0, 0)));
			}

			return true;
		}

		private LocalAdsModel getLocalAds(){
			Manager.GetInstance();
			int _totalWeight = getTotalViewWieght();
			int _selectWeight = Random.Range(0, _totalWeight);
			int _sumWeight = 0;

			for (int i = 0; i < Manager.LocalAdsTable.Count; i++) {
				if (Manager.LocalAdsTable[i].IsLoaded()) {
					_sumWeight += Manager.LocalAdsTable[i].ViewWeight.AsInt;
					if (_sumWeight >= _selectWeight) {
						return Manager.LocalAdsTable[i];
					}
				}
			}

			return null;
		}

		private int getTotalViewWieght(){
			int _totalViewWiehgt = Manager.LocalAdsTable.Where(_row => _row.IsLoaded()).Sum(_row => _row.ViewWeight.AsInt);
			return _totalViewWiehgt;
		}

		public void ShowBanner()
		{
			StartCoroutine(changeBanner());
		}

		public void HideBanner(){
			StopCoroutine(changeBanner());
		}


		private IEnumerator changeBanner(){
			while (true) {
				
				changeSprite ();
				changeMent ();
				yield return new WaitForSeconds(localAds.BannerInterval.AsFloat);
			}
		}

		private int spriteCounter = 0;
		private void changeSprite(){
			holder.BannerImages.sprite = bannerSprites[spriteCounter];

			spriteCounter++;
			if (spriteCounter >= bannerSprites.Count) {
				spriteCounter = 0;
			}
		}

		private int mentCounter = 0;
		private float mentIntervalCounter = 0;
		private void changeMent(){

			if (holder.MentText != null && localAds.Ment.Count > 0) {
				holder.MentText.text = localAds.Ment [mentCounter].AsString;
			}

			if (mentIntervalCounter > localAds.MentInterval.AsFloat) {
				mentCounter++;
				if (mentCounter >= localAds.Ment.Count) {
					mentCounter = 0;
				}
				mentIntervalCounter = 0;
			}

			mentIntervalCounter += localAds.BannerInterval.AsFloat;
		}

		public void MoveToStore(string _campainName){
			string _appID = localAds.Id.AsString;

			#if UNITY_EDITOR
			Application.OpenURL("https://play.google.com/store/apps/details?id="+_appID);
			#elif UNITY_IOS
			Application.OpenURL ("itms-apps://itunes.apple.com/app/id" + _appID);
			#elif UNITY_ANDROID
			Application.OpenURL("market://details?id="+_appID+"&referrer=utm_source%3Dbigjam%26utm_campaign%3D"+_campainName);
			#endif
		}

		public string GetAppId(){
			return localAds.Id.AsString;
		}
	}


}