﻿using UnityEngine;
using System.Collections;
using BicDB;
using BicDB.Variable;
using System;
using BicDB.Storage;
using System.Linq;
using BicDB.Container;
using BicUtil.ResourceDownloader;

namespace LocalAds
{
	public class Manager : MonoBehaviour {
		
		#region singleton
		private static Manager instance = null;  
		private static GameObject container;  
		public static Manager GetInstance()  
		{  
			if(instance == null)  
			{  
				container = new GameObject();  
				container.name = "LocalAdsManager";  
				instance = container.AddComponent(typeof(Manager)) as Manager;  
				DontDestroyOnLoad(container);
				instance.setDatabase ();
			}  

			return instance;  
		}  
		#endregion

		#region Database
		static public ITableContainer<LocalAdsModel> LocalAdsTable;
		static public ITableContainer<LocalAdsResourceModel> LocalAdsResourceTable;
		private void setDatabase(){
			LocalAdsTable = BicDB.Manager.GetOrCreateTable<LocalAdsModel> ("localAdsTable");
			var _url = "https://allapp-7c9c4.firebaseio.com/localAds/ver1/" + Application.identifier.Replace(".", "") + "/" + Application.platform.ToString() + ".json";

			var _syncStorage = new SyncStorage ();
			_syncStorage.SetEncryptKey ("localads");
			LocalAdsTable.Header[SyncStorage.LOAD_URL_KEY] = new StringVariable(_url);
			LocalAdsTable.PrimaryKey = "id";
			LocalAdsTable.SetStorage (_syncStorage);

			LocalAdsResourceTable = BicDB.Manager.GetOrCreateTable<LocalAdsResourceModel> ("localAdsResource");
			LocalAdsResourceTable.SetStorage (_syncStorage);
			LocalAdsResourceTable.Load ();
		}
		#endregion

		private float reloadInterval = 2f;
		public void LoadByServer(){
			LocalAdsTable.Load (onLoadedData, new SyncStorageParameter(SyncStorageParameter.SyncMode.All, SyncStorageParameter.SyncTarget.All));
		}

		private void onLoadedData(Result _result){
			if (_result.Code == (int)SyncStorage.ResultCode.Success) {
				LocalAdsTable.Save ();
				downloadResource ();
			} else {
				LeanTween.delayedCall (reloadInterval, LoadByServer);
				reloadInterval *= 2f;
			}
		}

		private void downloadResource(){
			
			for (int i = 0; i < LocalAdsTable.Count; i++) {
				for (int j = 0; j < LocalAdsTable [i].Images.Count; j++) {
					string _url = LocalAdsTable [i].Images [j].AsString;
					var _item = LocalAdsResourceTable.FirstOrDefault (_row => _row.Url.AsString == _url);
					if (_item == null) {
						ResourceDownloader.GetInstance ().DownlaodAndSaveImage (_url, Guid.NewGuid ().ToString () + ".png", onFinishedDownloadResource);
					}
				}
			}
		}

		void onFinishedDownloadResource (ResourceDownloader.ResultParam _param)
		{
			if (_param.IsSuccess) {
				saveResourceInfo (_param.Url, _param.SavePath);
			}
		}

		void saveResourceInfo(string _url, string _filePath){
			var _row = new LocalAdsResourceModel ();
			_row.FilePath.AsString = _filePath;
			_row.Url.AsString = _url;
			LocalAdsResourceTable.Add (_row);
			LocalAdsResourceTable.Save ();
		}
	}
}
